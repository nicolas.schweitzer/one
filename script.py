#!/usr/bin/env python3

def main():
    test_url = "https://app.datadoghq.com/ci/test-runs?query=test_level%3Atest%20%40test.name%3A%22TestWindowsSecretSuite%22%20%40test.suite%3A%22github.com%2FDataDog%2Fdatadog-agent%2Ftest%2Fnew-e2e%2Ftests%2Fagent-subcommands%2Fsecret%22%20%40git.branch%3Amain%20%40test.service%3Adatadog-agent&agg_m=count&agg_m_source=base&agg_t=count&fromUser=false&index=citest&start=1724316913304&end=1724921713304&paused=false"
    print(f"URL tentative {test_url}")
    href = f'<a href="{test_url}">hyperlink</a>'
    print(f"another tentative {href}")
    print("  See this test name on main in Test Visibility at https://app.datadoghq.com/ci/test-runs?query=test_level%3Atest%20%40test.name%3A%22TestWindowsSecretSuite%2FTestAgentSecretChecksExecutablePermissions%22%20%40test.suite%3A%22github.com%2FDataDog%2Fdatadog-agent%2Ftest%2Fnew-e2e%2Ftests%2Fagent-subcommands%2Fsecret%22%20%40git.branch%3Amain%20%40test.service%3Adatadog-agent\n")

if __name__ == "__main__":
    main()
